module Route exposing (Route(..), parseUrl, pushUrl, toString)

import Browser.Navigation as BN
import Url exposing (Url)
import Url.Parser as UP exposing (Parser, (</>))


type Route =
    NotFound
    | ListThreads
    | NewThread
    | DisplayThread Int


parseUrl : Url -> Route
parseUrl url =
    case UP.parse matchRoute url of
        Just route ->
            route

        Nothing ->
            NotFound


pushUrl : Route -> BN.Key -> Cmd msg
pushUrl route navKey =
    BN.pushUrl navKey (toString route)


toString : Route -> String
toString route =
    case route of
        NotFound ->
            "/not-found"

        ListThreads ->
            "/threads"

        NewThread ->
            "/threads/new"

        DisplayThread threadID ->
            "/threads/" ++ (String.fromInt threadID)


matchRoute : Parser (Route -> a) a
matchRoute =
    UP.oneOf
        [ UP.map ListThreads UP.top
        , UP.map ListThreads (UP.s "threads")
        , UP.map NewThread (UP.s "threads" </> UP.s "new")
        , UP.map DisplayThread (UP.s "threads" </> UP.int)
        ]
