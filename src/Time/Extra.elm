module Time.Extra exposing (posixToString, toEnglishMonth, posixDecoder, posixEncoder)

import Json.Decode as JD
import Json.Decode.Pipeline as JDP
import Json.Encode as JE
import String.Extra as SE
import Time as T


posixToString : T.Posix -> T.Zone -> String
posixToString posix timezone =
    let
        year =
            String.fromInt (T.toYear timezone posix)

        day =
            String.fromInt (T.toDay timezone posix)

        month =
            (T.toMonth timezone posix
                |> toEnglishMonth
                |> SE.toSentenceCase
                |> String.slice 0 3
            ) ++ "."

        hour =
            T.toHour timezone posix
                |> String.fromInt
                |> (++) "0"
                |> String.reverse
                |> String.slice 0 2
                |> String.reverse

        minute =
            T.toMinute timezone posix
                |> String.fromInt
                |> (++) "0"
                |> String.reverse
                |> String.slice 0 2
                |> String.reverse

    in
    day ++ " " ++  month ++ " " ++ year ++ " " ++ hour ++ ":" ++ minute

toEnglishMonth : T.Month -> String
toEnglishMonth month =
    case month of
        T.Jan -> "january"
        T.Feb -> "february"
        T.Mar -> "march"
        T.Apr -> "april"
        T.May -> "may"
        T.Jun -> "june"
        T.Jul -> "july"
        T.Aug -> "august"
        T.Sep -> "september"
        T.Oct -> "october"
        T.Nov -> "november"
        T.Dec -> "december"


posixDecoder : JD.Decoder T.Posix
posixDecoder =
    JDP.custom JD.int (JD.succeed T.millisToPosix)


posixEncoder : T.Posix -> JE.Value
posixEncoder posix =
    JE.int (T.posixToMillis posix)
