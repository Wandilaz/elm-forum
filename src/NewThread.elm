module NewThread exposing (Model, Msg, init, view, update)

import Browser.Navigation as BN
import Environment as Env
import Error as E
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http
import Route as R
import Thread as T exposing (Thread)


type alias Model =
    { navKey : BN.Key
    , thread : Thread
    , error : Maybe String
    }


type Msg =
    StoreTitle String
    | CreateThread
    | ThreadCreated (Result Http.Error Thread)


init : BN.Key -> ( Model, Cmd Msg )
init navKey =
    (   { navKey = navKey
        , thread = T.emptyThread
        , error = Nothing }
    , Cmd.none
    )


view : Model -> Html Msg
view model =
    div [ class "container-fluid" ]
        [ h1 [ class "text-center font-weight-bold" ]
            [ text "Create a new thread" ]
        , hr [] []
        , E.viewError "Unable to create a thread at this time."  model.error
        , label [ for "title" ] [ text "Title: " ]
        , input
            [ type_ "text"
            , class "form-control"
            , id "title"
            , onInput StoreTitle
            ] []
        , hr [] []
        , div [ class "text-right" ]
            [ a [ class "btn btn-light", href "/threads" ] [ text "Cancel" ]
            , button
                [ type_ "button"
                , class "btn btn-success ml-1"
                , onClick CreateThread
                ]
                [ text "Create" ]
            ]
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StoreTitle title ->
            let
                oldThread =
                    model.thread

                newThread =
                    { oldThread | title = title }
            in
            ( { model | thread = newThread }, Cmd.none )

        CreateThread ->
            ( model, createThread model.thread )

        ThreadCreated (Ok thread) ->
            ( { model | error = Nothing }, R.pushUrl R.ListThreads model.navKey )

        ThreadCreated (Err error) ->
            ( { model | error = Just (E.getMessage error) }, Cmd.none )


createThread : Thread -> Cmd Msg
createThread thread =
    Http.post
        { url = Env.dbUrl ++ "/threads"
        , body = Http.jsonBody (T.threadEncoder thread)
        , expect = Http.expectJson ThreadCreated T.threadDecoder
        }
