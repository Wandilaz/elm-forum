module DisplayThread exposing (Model, Msg, init, view, update)

import Browser.Navigation as BN
import Error as E
import Environment as Env
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Http
import Post as P exposing (Post)
import RemoteData as RD
import Route as R
import Task as Ta
import Thread as Th exposing (Thread)
import Time as Ti
import Time.Extra as TiE

type alias Model =
    { navKey : BN.Key
    , threadID : Int
    , thread : RD.WebData Thread
    , timezone : Ti.Zone
    , newMessage : String
    , newAuthor : String
    , postCreationError : Maybe String
    }


type Msg =
    ThreadReceived (RD.WebData Thread)
    | TimezoneReceived Ti.Zone
    | StoreMessage String
    | StoreAuthor String
    | FetchPostCreationTime
    | PostCreationTimeReceived Ti.Posix
    | PostCreated (Result Http.Error Post)


init : BN.Key -> Int -> ( Model, Cmd Msg )
init navKey threadID =
    (   { navKey = navKey
        , threadID = threadID
        , thread = RD.Loading
        , timezone = Ti.utc
        , newMessage = ""
        , newAuthor = ""
        , postCreationError = Nothing
        }
    , fetchThread threadID
    )


view : Model -> Html Msg
view model =
    case model.thread of
        RD.NotAsked ->
            text ""

        RD.Loading ->
            div [ class "container-fluid" ]
                [ h1
                    [ class "text-center font-weight-bold" ]
                    [ text ("Thread #" ++ (String.fromInt model.threadID)) ]
                , hr [] []
                , p [ class "alert alert-secondary" ] [ text "Loading..." ]
                ]

        RD.Success thread ->
            div [ class "container-fluid" ]
                [ h1
                    [ class "text-center font-weight-bold" ]
                    [ text thread.title ]
                , hr [] []
                , div
                    [ class "list-group" ]
                    (List.map (viewPost model.timezone) thread.posts)
                , hr [] []
                , E.viewError
                    "Unable to fetch the posts at this time."
                    model.postCreationError
                , label [ for "messageField" ] [ text "Message" ]
                , textarea
                    [ class "form-control"
                    , id "messageField"
                    , placeholder "Your message..."
                    , onInput StoreMessage
                    ]
                    []
                , label
                    [ for "authorField" ]
                    [ text "Username "
                    , small [] [ text "(Optional)" ]
                    ]
                , input
                    [ class "form-control"
                    , id "authorField"
                    , onInput StoreAuthor
                    ]
                    []
                , div
                    [ class "text-right mt-1" ]
                    [ button
                        [ class "btn btn-success"
                        , onClick FetchPostCreationTime
                        ]
                        [ text "Submit" ]
                    ]
                ]

        RD.Failure httpError ->
            div [ class "container-fluid" ]
                [ h1
                    [ class "text-center font-weight-bold" ]
                    [ text ("Thread #" ++ (String.fromInt model.threadID)) ]
                , hr [] []
                , E.viewError
                    "Unable to create a post at this time."
                    (Just (E.getMessage httpError))
                ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ThreadReceived response ->
            ( { model | thread = response }
            , Ta.perform TimezoneReceived Ti.here
            )

        TimezoneReceived timezone ->
            ( { model | timezone = timezone }, Cmd.none )

        StoreMessage message ->
            ( { model | newMessage = message }, Cmd.none )

        StoreAuthor author ->
            ( { model | newAuthor = author }, Cmd.none )

        FetchPostCreationTime ->
            ( model, Ta.perform PostCreationTimeReceived Ti.now )

        PostCreationTimeReceived time ->
            ( model
            , createPost model.threadID model.newMessage model.newAuthor time
            )

        PostCreated (Ok post) ->
            ( { model | postCreationError = Nothing }
            , R.pushUrl (R.DisplayThread model.threadID) model.navKey
            )

        PostCreated (Err error) ->
            ( { model | postCreationError = Just (E.getMessage error) }
            , Cmd.none
            )


fetchThread : Int -> Cmd Msg
fetchThread threadID =
    Http.get
        { url = Env.dbUrl
            ++ "/threads/"
            ++ (String.fromInt threadID)
            ++ "?_embed=posts"
        , expect =
            Http.expectJson (RD.fromResult >> ThreadReceived) Th.threadDecoder
        }


viewPost : Ti.Zone -> Post -> Html Msg
viewPost timezone post =
    let
        italicAuthor =
            case post.author of
                Just author ->
                    ""

                Nothing ->
                    " font-italic"
    in
    div [ class "list-group-item mb-0" ]
        [ small
            [ class ("font-weight-bold" ++ italicAuthor) ]
            [ text (Maybe.withDefault "Anonymous" post.author) ]
        , p [] [ text post.message ]
        , div [ class "text-right" ]
            [ small
                [ class "text-muted" ]
                [ text (TiE.posixToString post.date timezone)]
            ]
        ]


createPost : Int -> String -> String -> Ti.Posix -> Cmd Msg
createPost threadID message author time =
    let
        emptyPost = P.emptyPost

        maybeAuthor =
            if (String.isEmpty author) then
                Nothing
            else
                Just author

        newPost =
            { emptyPost
                | message = message
                , author = maybeAuthor
                , date = time
                , threadID = threadID
            }
    in
    Http.post
        { url = Env.dbUrl ++ "/posts"
        , body = Http.jsonBody (P.postEncoder newPost)
        , expect = Http.expectJson PostCreated P.postDecoder
        }
