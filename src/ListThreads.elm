module ListThreads exposing (Model, Msg, init, view, update)

import Error as E
import Environment as Env
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Decode as JD
import RemoteData as RD
import Thread as T exposing (Thread)


type alias Model =
    { threads : RD.WebData (List Thread)
    }


type Msg =
    ThreadsReceived (RD.WebData (List Thread))


init : ( Model, Cmd Msg )
init =
    ( { threads = RD.Loading }, fetchThreads )


view : Model -> Html Msg
view model =
    div [ class "container-fluid" ]
        [ h1  [ class "text-center font-weight-bold" ] [ text "Threads list" ]
        , hr [] []
        , a
            [ class "btn btn-primary btn-lg btn-block mb-3"
            , href "/threads/new"
            ]
            [ text "New thread" ]
        , viewThreads model.threads
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ThreadsReceived response ->
            ( { model | threads = response }, Cmd.none )


fetchThreads : Cmd Msg
fetchThreads =
    Http.get
        { url = Env.dbUrl ++ "/threads"
        , expect =
            Http.expectJson
                (RD.fromResult >> ThreadsReceived)
                (JD.list T.threadDecoder)
        }


viewThreads : RD.WebData (List Thread) -> Html Msg
viewThreads threadsData =
    case threadsData of
        RD.NotAsked ->
            text ""

        RD.Loading ->
            h5 [ class "alert alert-secondary" ] [ text "Loading..." ]

        RD.Success threads ->
            div [ class "list-group" ] (List.map viewThread threads)

        RD.Failure httpError ->
            E.viewError
                "Unable to fetch the threads at this time."
                (Just (E.getMessage httpError))


viewThread : Thread -> Html Msg
viewThread thread =
    h5 [ class "list-group-item mb-0" ]
        [ a [ class "text-dark"
            , href ("/threads/" ++ (String.fromInt thread.id))
            ]
            [ text thread.title ]
        ]
