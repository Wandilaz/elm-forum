module Post exposing (Post, postDecoder, postEncoder, emptyPost)

import Json.Decode as JD
import Json.Decode.Pipeline as JDP
import Json.Encode as JE
import Time as T
import Time.Extra as TE


emptyID : Int
emptyID = -1


type alias Post =
    { id : Int
    , message : String
    , author : Maybe String
    , date : T.Posix
    , threadID : Int
    }


postDecoder : JD.Decoder Post
postDecoder =
    let
        maybeStringDecoder =
            JDP.custom JD.string (JD.succeed Just)
    in
    JD.succeed Post
        |> JDP.required "id" JD.int
        |> JDP.required "message" JD.string
        |> JDP.optional "author" maybeStringDecoder Nothing
        |> JDP.required "date" TE.posixDecoder
        |> JDP.required "threadId" JD.int


postEncoder : Post -> JE.Value
postEncoder post =
    let
        idEncoder =
            if post.id /= emptyID then
                [ ( "id", JE.int post.id ) ]
            else
                []

        authorEncoder =
            case post.author of
                Just author ->
                    [ ( "author", JE.string author ) ]

                Nothing ->
                    [ ( "author", JE.null ) ]

    in
    JE.object (idEncoder ++
        [ ( "message", JE.string post.message ) ]
        ++ authorEncoder ++
        [ ( "date", TE.posixEncoder post.date )
        , ( "threadId", JE.int post.threadID )
        ])


emptyPost : Post
emptyPost =
    { id = emptyID
    , message = ""
    , author = Nothing
    , date = T.millisToPosix 0
    , threadID = emptyID
    }
