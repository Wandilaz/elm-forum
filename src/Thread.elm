module Thread exposing (Thread, threadDecoder, threadEncoder, emptyThread)

import Json.Decode as JD
import Json.Decode.Pipeline as JDP
import Json.Encode as JE
import Post as P exposing (Post)


emptyID : Int
emptyID = -1


type alias Thread =
    { id : Int
    , title : String
    , posts : List Post
    }


threadDecoder : JD.Decoder Thread
threadDecoder =
    JD.succeed Thread
        |> JDP.required "id" JD.int
        |> JDP.required "title" JD.string
        |> JDP.optional "posts" (JD.list P.postDecoder) []

threadEncoder : Thread -> JE.Value
threadEncoder thread =
    let
        idEncoder =
            if thread.id /= emptyID then
                [ ( "id", JE.int thread.id ) ]
            else
                []

    in
    JE.object (idEncoder ++
        [ ( "title", JE.string thread.title )
        ]
    )


emptyThread : Thread
emptyThread =
    { id = emptyID
    , title = ""
    , posts = []
    }
