module Error exposing (getMessage, viewError)

import Html exposing (..)
import Html.Attributes exposing (..)
import Http

getMessage : Http.Error -> String
getMessage httpError =
    case httpError of
        Http.BadUrl message ->
            message

        Http.Timeout ->
            "The server is taking too long to respond. Please try again later."

        Http.NetworkError ->
            "Unable to reach the server."

        Http.BadStatus statusCode ->
            "Request failed with status code: " ++ String.fromInt statusCode

        Http.BadBody message ->
            message


viewError : String -> Maybe String -> Html msg
viewError headerMessage errorData =
    case errorData of
        Just error ->
            div [ class "alert alert-danger" ]
                [ h5 [] [ text headerMessage ]
                , p [] [ text ("Error: " ++ error) ]
                ]

        Nothing ->
            text ""
