module Main exposing (main)

import Browser as B
import Browser.Navigation as BN
import DisplayThread as DT
import Html exposing (..)
import Html.Attributes exposing (..)
import ListThreads as LT
import NewThread as NT
import Route as R
import Url exposing (Url)


type Msg =
    ListThreadsMsg LT.Msg
    | NewThreadMsg NT.Msg
    | DisplayThreadMsg DT.Msg
    | LinkClicked B.UrlRequest
    | UrlChanged Url


type alias Model =
    { navKey : BN.Key
    , pageModel : PageModel
    }


type PageModel =
    NotFound
    | ListThreadsModel LT.Model
    | NewThreadModel NT.Model
    | DisplayThreadModel DT.Model


main : Program () Model Msg
main =
    B.application
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }


init : () -> Url -> BN.Key -> ( Model, Cmd Msg )
init flags url navKey =
    case R.parseUrl url of
        R.NotFound ->
            ( { navKey = navKey, pageModel = NotFound }
            , Cmd.none
            )

        R.ListThreads ->
            let
                ( ltModel, ltCmd ) =
                    LT.init
            in
            ( { navKey = navKey, pageModel = ListThreadsModel ltModel }
            , Cmd.map ListThreadsMsg ltCmd
            )

        R.NewThread ->
            let
                ( ntModel, ntCmd ) =
                    NT.init navKey
            in
            ( { navKey = navKey, pageModel = NewThreadModel ntModel }
            , Cmd.map NewThreadMsg ntCmd
            )

        R.DisplayThread threadID ->
            let
                ( dtModel, dtCmd ) =
                    DT.init navKey threadID
            in
            ( { navKey = navKey, pageModel = DisplayThreadModel dtModel }
            , Cmd.map DisplayThreadMsg dtCmd
            )


view : Model -> B.Document Msg
view model =
    let
        currentView =
            case model.pageModel of
                NotFound ->
                    { title = "Page not found"
                    , body =
                        [ h3 [] [ text "The page you requested was not found." ]
                        ]
                    }

                ListThreadsModel ltModel ->
                    { title = "Threads list"
                    , body = [ Html.map ListThreadsMsg (LT.view ltModel) ]
                    }

                NewThreadModel ntModel ->
                    { title = "Create a new thread"
                    , body = [ Html.map NewThreadMsg (NT.view ntModel) ]
                    }

                DisplayThreadModel dtModel ->
                    { title = "Thread #" ++ (String.fromInt dtModel.threadID)
                    , body = [ Html.map DisplayThreadMsg (DT.view dtModel) ]
                    }
    in
    { currentView | body = (currentView.body ++ [ addMadeWithElm ]) }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( model.pageModel, msg ) of
        ( ListThreadsModel ltModel, ListThreadsMsg ltMsg ) ->
            let
                ( updatedModel, updatedCmd ) =
                    LT.update ltMsg ltModel
            in
            ( { model | pageModel = ListThreadsModel updatedModel }
            , Cmd.map ListThreadsMsg updatedCmd
            )

        ( NewThreadModel ntModel, NewThreadMsg ntMsg ) ->
            let
                ( updatedModel, updatedCmd ) =
                    NT.update ntMsg ntModel
            in
            ( { model | pageModel = NewThreadModel updatedModel }
            , Cmd.map NewThreadMsg updatedCmd
            )

        ( DisplayThreadModel dtModel, DisplayThreadMsg dtMsg ) ->
            let
                ( updatedModel, updatedCmd ) =
                    DT.update dtMsg dtModel
            in
            ( { model | pageModel = DisplayThreadModel updatedModel }
            , Cmd.map DisplayThreadMsg updatedCmd
            )

        ( _, LinkClicked urlRequest ) ->
            case urlRequest of
                B.Internal url ->
                    ( model
                    , BN.pushUrl model.navKey (Url.toString url)
                    )

                B.External url ->
                    ( model, BN.load url )

        ( _, UrlChanged url ) ->
            init () url model.navKey

        ( _, _ ) ->
            ( model, Cmd.none )


addMadeWithElm : Html Msg
addMadeWithElm =
    p [ class "container-fluid mt-5 text-right" ]
        [ small [ class "font-italic" ]
            [ text "Made with "
            , strong [] [ text "Elm " ]
            ]
        , img
            [ class "img-fluid"
            , src "assets/elm_logo.png"
            , alt "Elm logo"
            , width 20
            ]
            []
        ]
