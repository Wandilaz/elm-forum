FROM nginx

COPY index.html /usr/share/nginx/html
COPY elm.js /usr/share/nginx/html
COPY assets /usr/share/nginx/html/assets

RUN sed -i '$i sed -i "s!<DB_URL>!$DB_URL!g" /usr/share/nginx/html/elm.js' \
/docker-entrypoint.sh

EXPOSE 80
