module PostTest exposing (postDecoderTest, postEncoderTest)

import Expect as E
import Json.Decode as JD
import Json.Encode as JE
import Post as P exposing (Post)
import String.Extra as SE
import Test exposing (..)
import Time as T


postDecoderTest : Test
postDecoderTest =
    describe "postDecoder"
        [ test (SE.unindent
            """
                Given a valid JSON string input representing a post,
                then return a corresponding value of type Post.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 72,
                              "message": "A message",
                              "author": "Someone",
                              "date": 3600000,
                              "threadId": 25
                            }
                        """)

                    output =
                        JD.decodeString P.postDecoder input

                in
                E.equal output
                    (Ok
                        { id = 72
                        , message = "A message"
                        , author = Just "Someone"
                        , date = T.millisToPosix 3600000
                        , threadID = 25
                        }
                    )

        , test (SE.unindent
            """
                Given a valid JSON string input representing a post with a
                null value in the "author" field,
                then return a corresponding value of type Post.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 72,
                              "message": "A message",
                              "author": null,
                              "date": 3600000,
                              "threadId": 25
                            }
                        """)

                    output =
                        JD.decodeString P.postDecoder input

                in
                E.equal output
                    (Ok
                        { id = 72
                        , message = "A message"
                        , author = Nothing
                        , date = T.millisToPosix 3600000
                        , threadID = 25
                        }
                    )

        , test (SE.unindent
            """
                Given a wrong JSON input with a string in the "id" field,
                then return an error.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": "72",
                              "message": "A message",
                              "author": "Someone",
                              "date": 3600000,
                              "threadId": 25
                            }
                        """)

                    output =
                        JD.decodeString P.postDecoder input

                in
                E.err output

        , test (SE.unindent
            """
                Given a wrong JSON input with a null value in the "message"
                field,
                then return an error.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 72,
                              "message": null,
                              "author": "Someone",
                              "date": 3600000,
                              "threadId": 25
                            }
                        """)

                    output =
                        JD.decodeString P.postDecoder input

                in
                E.err output

        , test (SE.unindent
            """
                Given a wrong JSON input with a number in the "author" field,
                then return an error.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 72,
                              "message": "A message",
                              "author": 1337,
                              "date": 3600000,
                              "threadId": 25
                            }
                        """)

                    output =
                        JD.decodeString P.postDecoder input

                in
                E.err output

        , test (SE.unindent
            """
                Given a wrong JSON input with a string in the "date" field,
                then return an error.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 72,
                              "message": "A message,
                              "author": "Someone",
                              "date": "3600000",
                              "threadId": 25
                            }
                        """)

                    output =
                        JD.decodeString P.postDecoder input

                in
                E.err output

        , test (SE.unindent
            """
                Given a wrong JSON input with a string in the "threadId" field,
                then return an error.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 72,
                              "message": "A message",
                              "author": 1337,
                              "date": 3600000,
                              "threadId": "25"
                            }
                        """)

                    output =
                        JD.decodeString P.postDecoder input

                in
                E.err output
        ]

postEncoderTest : Test
postEncoderTest =
    describe "postEncoder"
        [ test (SE.unindent
            """
                Given a value of type Post,
                then return a corresponding JSON string.
            """) <|
            \() ->
                let
                    input =
                        { id = 72
                        , message = "A message"
                        , author = Just "Someone"
                        , date = T.millisToPosix 3600000
                        , threadID = 25
                        }

                    output =
                        "\n" ++ (JE.encode 2 (P.postEncoder input)) ++ "\n"

                in
                E.equal output (SE.unindent
                    """
                        {
                          "id": 72,
                          "message": "A message",
                          "author": "Someone",
                          "date": 3600000,
                          "threadId": 25
                        }
                    """)

        , test (SE.unindent
            """
                Given a value of type Post with Nothing in the "author" field,
                then return a corresponding JSON string.
            """) <|
            \() ->
                let
                    input =
                        { id = 72
                        , message = "A message"
                        , author = Nothing
                        , date = T.millisToPosix 3600000
                        , threadID = 25
                        }

                    output =
                        "\n" ++ (JE.encode 2 (P.postEncoder input)) ++ "\n"

                in
                E.equal output (SE.unindent
                    """
                        {
                          "id": 72,
                          "message": "A message",
                          "author": null,
                          "date": 3600000,
                          "threadId": 25
                        }
                    """)
        ]
