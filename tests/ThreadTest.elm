module ThreadTest exposing (threadDecoderTest, threadEncoderTest)

import Expect as E
import Json.Decode as JD
import Json.Encode as JE
import String.Extra as SE
import Test exposing (..)
import Thread as T


threadDecoderTest : Test
threadDecoderTest =
    describe "threadDecoder"
        [ test (SE.unindent
            """
                Given a valid JSON string input representing a thread,
                then return a corresponding value of type Thread.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 548,
                              "title": "A title"
                            }
                        """)

                    output =
                        JD.decodeString T.threadDecoder input

                in
                E.equal output
                    (Ok
                        { id = 548
                        , title = "A title"
                        , posts = []
                        }
                    )

        , test (SE.unindent
            """
                Given a wrong JSON input with a string in the "id" field,
                then return an error.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": "548",
                              "title": "A title"
                            }
                        """)

                    output =
                        JD.decodeString T.threadDecoder input

                in
                E.err output

        , test (SE.unindent
            """
                Given a wrong JSON input with a number in the "title" field,
                then return an error.
            """) <|
            \() ->
                let
                    input = (SE.unindent
                        """
                            {
                              "id": 548,
                              "title": 713705
                            }
                        """)

                    output =
                        JD.decodeString T.threadDecoder input

                in
                E.err output
        ]

threadEncoderTest : Test
threadEncoderTest =
    describe "threadEncoder"
        [ test (SE.unindent
            """
                Given a value of type Thread,
                then return a corresponding JSON string.
            """) <|
            \() ->
                let
                    input =
                        { id = 548
                        , title = "A title"
                        , posts = []
                        }

                    output =
                        "\n" ++ (JE.encode 2 (T.threadEncoder input)) ++ "\n"

                in
                E.equal output (SE.unindent
                    """
                        {
                          "id": 548,
                          "title": "A title"
                        }
                    """)
        ]
