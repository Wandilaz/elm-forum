module RouteTest exposing (parseUrlTest)

import Expect as E
import Route as R
import String.Extra as SE
import Test exposing (..)
import Url exposing (Url)


defaultUrl : Url
defaultUrl =
    { protocol = Url.Http
    , host = ""
    , port_ = Nothing
    , path = ""
    , query = Nothing
    , fragment = Nothing
    }


parseUrlTest : Test
parseUrlTest =
    describe "parseUrl"
        [ test (SE.unindent
            """
                Given an invalid url,
                then return R.NotFound.
            """) <|
            \() ->
                let
                    input =
                        Url.fromString "http://localhost/fakeRoute"
                            |> Maybe.withDefault defaultUrl

                    output =
                        R.parseUrl input

                in
                E.equal output R.NotFound

        , test (SE.unindent
            """
                Given the top url,
                then return R.ListThreads.
            """) <|
            \() ->
                let
                    input =
                        Url.fromString "http://localhost"
                            |> Maybe.withDefault defaultUrl

                    output =
                        R.parseUrl input

                in
                E.equal output R.ListThreads

        , test (SE.unindent
            """
                Given the "/threads" url,
                then return R.ListThreads.
            """) <|
            \() ->
                let
                    input =
                        Url.fromString "http://localhost/threads"
                            |> Maybe.withDefault defaultUrl

                    output =
                        R.parseUrl input

                in
                E.equal output R.ListThreads

        , test (SE.unindent
            """
                Given the "/threads/new" url,
                then return R.NewThread.
            """) <|
            \() ->
                let
                    input =
                        Url.fromString "http://localhost/threads/new"
                            |> Maybe.withDefault defaultUrl

                    output =
                        R.parseUrl input

                in
                E.equal output R.NewThread

        , test (SE.unindent
            """
                Given the "/threads/651" url,
                then return R.DisplayThread 651.
            """) <|
            \() ->
                let
                    input =
                        Url.fromString "http://localhost/threads/651"
                            |> Maybe.withDefault defaultUrl

                    output =
                        R.parseUrl input

                in
                E.equal output (R.DisplayThread 651)
        ]
