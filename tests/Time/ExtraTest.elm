module Time.ExtraTest exposing (posixToStringTest)

import Expect as E
import String.Extra as SE
import Test exposing (..)
import Time as T
import Time.Extra as TE


posixToStringTest : Test
posixToStringTest =
    describe "posixToString"
        [ test (SE.unindent
            """
                Given the value T.Posix 1604356139000 and Zone 0 [] (UTC),
                then return the string "2 Nov 2020 22:28".
            """) <|
            \() ->
                let
                    inputPosix = T.millisToPosix 1604356139000
                    inputZone = T.utc

                    output =
                        TE.posixToString inputPosix inputZone

                in
                E.equal output "2 Nov. 2020 22:28"
        ]
